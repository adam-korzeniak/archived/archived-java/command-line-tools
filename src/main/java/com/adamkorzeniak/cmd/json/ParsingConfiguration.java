package com.adamkorzeniak.cmd.json;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Data
@Configuration
@ConfigurationProperties(prefix = "app.parser.file.location")
public class ParsingConfiguration {
    @NotBlank
    private String input;
    @NotBlank
    private String output;
}
